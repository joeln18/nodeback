
const getAllService = (Data, req, res, order) => {
    const { populate } = req.params;
    const { key, value } = req.query;
    const validPopulate = populate == "nopopulate" ? "" : populate;

    let query = { estado: true };
    if(key != undefined && value != undefined)
        query = { [key]: value, ...query}

    console.log("key ", query);
    console.log("validPopulate ", validPopulate);
    
    Data.find(query)
        .populate(validPopulate)
        .sort(order).exec((error, data)=>{
        if(error){
            return res.json({
                codigo: 400,
                success: false,
                error
            })
        }else{
            //console.log("data ", data);

            res.json({
                success: true,
                message: "Consulta exitosa",
                data,
            })
        }
    })
}

const disableService = (Data, req, res) =>{
    try{
        const { id, disable } = req.body;
        
        Data.findByIdAndUpdate(id, { estado: !disable },
            (error) => {
                if(error){
                    return res.json({
                        codigo: 400,
                        success: false,
                        error
                    })
                }else{
                    res.json({
                        success: true,
                        message: `Elemento ${disable ? "deshabilitado" : "habilitado"} exitosamente`
                    })
                }
            }
        )
    
    }catch(error){
        return res.json({
            codigo: 400,
            success: false,
            error
        });
    }
}

const updateService = (Data, req, res) => {
    try{
        const { id } = req.body;
        const update = req.body;
        
        Data.findByIdAndUpdate(id,update,
            (error) => {
                if(error){
                    return res.json({
                        codigo: 400,
                        success: false,
                        error
                    })
                }else{
                    res.json({
                        success: true,
                        message: "Actualización exitosa"
                    })
                }
            }
        )
    
    }catch(error){
        return res.json({
            codigo: 400,
            success: false,
            error
        });
    }
}

const createService = (Data, req, res) => {
    const requestBody = req.body;

    const data = new Data(requestBody);

    data.save(function(error, saved){
        console.log("saved ", saved);
        console.log("error ", error);
        if(error)
            return res.json({
                codigo: 400,
                success: false,
                error
            })
        else
            res.json({
                success: true,
                message: "Solicitud exitosa",
                _id: saved._id
            })
        
    });  
}

const getCourseService = (Data, req, res, order) => {
    const { populate } = req.params;
    const { key, value } = req.query;
    const validPopulate = populate == "nopopulate" ? "" : populate;

    let query = { estado: true };
    if(key != undefined && value != undefined)
        query = { [key]: value, ...query}
    
    Data.find(query)
        .populate("periodo")
        .populate(validPopulate)
        .sort(order).exec((error, data)=>{
            
        if(error){
            return res.json({
                codigo: 400,
                success: false,
                error
            })
        }else{
            //console.log("data ", data);

            res.json({
                success: true,
                message: "Consulta exitosa",
                data,
            })
        }
    })
}

module.exports = {
    getAllService,
    disableService,
    updateService,
    createService,
    getCourseService
}