const { response } = require('express');


const validarArchivo = (req, res = response, next) => {
    let msg = '';
    if (!req.files || Object.keys(req.files).length === 0) {
        msg = "No hay archivo que subir";
    }else if(!req.files.archivo){
        msg = "Nombre del parametro del archivo incorrecto";
    }
    if(msg !== ''){
        return res.status(400).json({msg});
    }

    next();
}

module.exports = {
    validarArchivo
}