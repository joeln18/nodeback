

const dbValidators = require('./db-validators');
const generarJWT = require('./generar-jwt');
const uploadArchivo = require('./uploadArchivo');

module.exports = {
    ...dbValidators,
    ...generarJWT,
    ...uploadArchivo
}