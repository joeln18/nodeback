const Role = require('../models/role');
const Usuario = require('../models/usuario');

const esRoleValido = async(rol = '') => {

    const existeRol = await Role.findOne({ rol });
    if ( !existeRol ) {
        throw new Error(`El rol ${ rol } no está registrado en la BD`);
    }
    return true;
}

const emailExiste = async( correo = '' ) => {

    // Verificar si el correo existe
    const existeEmail = await Usuario.findOne({ correo });
    if ( existeEmail ) {
        throw new Error(`El correo: ${ correo }, ya está registrado`);
    }
    return true;
}

const existeUsuarioPorId = async( id ) => {

    // Verificar si el correo existe
    const existeUsuario = await Usuario.findById(id);
    if ( !existeUsuario ) {
        throw new Error(`El id no existe ${ id }`);
    }
    return true;
}

const coleccionesPermitidas = ( coleccion = '', colecciones = []) => {
    const resp = colecciones.includes( coleccion );
    if(!resp){
        throw new Error(`La colección ${coleccion} no es permitida`);
    }
    return true;
}



module.exports = {
    esRoleValido,
    emailExiste,
    existeUsuarioPorId,
    coleccionesPermitidas
}

