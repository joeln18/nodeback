const path = require('path');
const { v4: uuidv4 } = require('uuid');

const uploadArchivo = ( files, validExtension = ['png', 'jpg', 'jpeg', 'git'], directory = '' ) => {
    
    return new Promise((resolve, reject) => {
        
        const { archivo } = files;
    
        const nameSplit = archivo.name.split('.');
        const extension = nameSplit[ nameSplit.length - 1 ];

        if(!validExtension.includes(extension)){
            return reject(`La extensión ${extension} no es permitida. Extensiones validas ${validExtension}`);
        }

        const name = uuidv4() + '.' + extension;
        const uploadPath = path.join(__dirname, '../uploads/', directory, name);

        archivo.mv(uploadPath, (err) => {
            if (err) {
                return reject(err);
            }

            resolve( name );
        });
    })
}

module.exports = {
    uploadArchivo
}