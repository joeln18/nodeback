const { Router } = require('express');
const { check } = require('express-validator');


const { validarCampos } = require("../middlewares");

const { createRole } = require("../controllers/role");

const router = Router();



router.post('/create', [
    check('rol', 'El Rol es obligatorio').not().isEmpty(),
    validarCampos
], createRole);

module.exports = router;