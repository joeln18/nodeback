const { Router } = require('express');


const { validarCampos } = require("../middlewares");

const { create, getAll, update, disable } = require("../controllers/caracteristicaController");

const router = Router();



router.post('/create', [
    validarCampos
], create);

router.post('/update', [
    validarCampos
], update);

router.post('/disable', [
    validarCampos
], disable);

router.get('/list/:populate', getAll);

module.exports = router;