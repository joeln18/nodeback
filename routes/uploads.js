const { Router } = require('express');
const { check } = require('express-validator');

const {
    validarCampos,
    validarJWT,
    validarArchivo
} = require('../middlewares');

const { cargarArchivo, actualizarArchivo, showArchivo } = require("../controllers/uploads");
const { coleccionesPermitidas } = require('../helpers');



const router = Router();

router.post('/', validarArchivo, cargarArchivo);

router.put('/:coleccion/:id',
[
    validarArchivo,
    check('id', 'El id debe estar registrado').isMongoId(),
    check('coleccion').custom( c => coleccionesPermitidas ( c, ['users', 'roles'])),
    validarCampos
], actualizarArchivo)


router.get('/:coleccion/:id',
[
    check('id', 'El id debe estar registrado').isMongoId(),
    check('coleccion').custom( c => coleccionesPermitidas ( c, ['users', 'roles'])),
    validarCampos
], showArchivo)



module.exports = router;