const { response, request } = require('express');

const Data = require("../models/programaModel");
const Child = require("../models/asignaturaModel");
const { getAllService, disableService, updateService, createService } = require('../utils/transversalServices');

const create = (req = request, res = response) => {
      createService(Data, req, res);
}

const update = (req = request, res = response) => {
    updateService(Data, req, res);
}

const disable = (req = request, res = response) => {
    Child.find({ $and: [ { estado: true }, { idPrograma: req.body.id }] }).exec((error, data)=>{
        if(error){
            return res.json({
                codigo: 400,
                success: false,
                error
            })
        }else{
           if(data.length > 0)
                return res.json({
                    codigo: 200,
                    success: false,
                    message: "Asignaturas creadas con el idPrograma enviado"
                })
            else
                disableService(Data, req, res);
        }
    })
}

const getAll = (req = request, res = response) => {
    getAllService(Data, req, res, { nombre: 1 });
}

module.exports = {
    create,
    getAll,
    update,
    disable
}