const { response, request } = require('express');

const Data = require("../models/periodoModel");
const { getAllService, disableService, updateService, createService } = require('../utils/transversalServices');

const create = (req = request, res = response) => {
      createService(Data, req, res);
}

const update = (req = request, res = response) => {
    updateService(Data, req, res);
}

const disable = (req = request, res = response) => {
    disableService(Data, req, res);
}

const getAll = (req = request, res = response) => {
    getAllService(Data, req, res, { codigo: 1 });
}

module.exports = {
    create,
    getAll,
    update,
    disable
}