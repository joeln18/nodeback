const  path  = require('path');
const fs = require('fs');
const { response } = require('express');
const { uploadArchivo } = require('../helpers');

const Usuario  = require('../models/usuario');


const cargarArchivo = async (req, res = response) => {
    
    console.log('req.files >>>', req.files); // eslint-disable-line

    try{
        const path = await uploadArchivo( req.files, ['jpg', 'png', 'jpeg'], 'images' )
        res.json({
            path
        });
    }catch(error){
        res.status(400).json({error})
    }
    
}

const actualizarArchivo = async (req, res = response) => {
    const { id, coleccion } = req.params;

    let modelo;

    switch (coleccion) {
        case 'users':

            modelo = await Usuario.findById(id);
            if(!modelo){
                res.status(400).json({
                    msg: 'No existe un usuario con el id ' + id
                });
            }



            break;
    
        default:
            return res.status(500).json({msg: 'Colección no validada'})
    }

    //Limpiar imagen del servidor
    if( modelo.img ){
        const pathImagen = path.join(__dirname, '../uploads', coleccion, modelo.img);
        if( fs.existsSync(pathImagen) ){
            fs.unlinkSync(pathImagen);
        }
    }

    try{
        const name = await uploadArchivo(req.files, undefined, coleccion);
        modelo.img = name;

        await modelo.save();
        
        res.json({
            modelo
        });
    }catch(error){
        res.status(400).json({error});
    }

    
}

const showArchivo = async (req, res = response) => {
    const { id, coleccion } = req.params;

    let modelo;

    switch (coleccion) {
        case 'users':
            modelo = await Usuario.findById(id);
            if(!modelo){
                res.status(400).json({
                    msg: 'No existe un usuario con el id ' + id
                });
            }
            break;
    
        default:
            return res.status(500).json({msg: 'Colección no validada'})
    }

    //Limpiar imagen del servidor
    if( modelo.img ){
        const pathImagen = path.join(__dirname, '../uploads', coleccion, modelo.img);
        if( fs.existsSync(pathImagen) ){
            return res.sendFile(pathImagen);
        }
    }

    res.sendFile(path.join(__dirname, '../assets', 'no-image.jpg'));

}

module.exports = {
    cargarArchivo,
    actualizarArchivo,
    showArchivo
}