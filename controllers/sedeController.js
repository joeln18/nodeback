const { response, request } = require('express');

const Data = require("../models/sedeModel");
const Child = require("../models/bloqueModel");
const { getAllService, disableService, updateService, createService } = require('../utils/transversalServices');

const create = (req = request, res = response) => {
      createService(Data, req, res);
}

const update = (req = request, res = response) => {
    updateService(Data, req, res);
}

const disable = (req = request, res = response) => {
    Child.find({ $and: [ { estado: true }, { idSede: req.body.id }] }).exec((error, data)=>{
        if(error){
            return res.json({
                codigo: 400,
                success: false,
                error
            })
        }else{
           if(data.length > 0)
                return res.json({
                    codigo: 200,
                    success: false,
                    message: "Bloques creados con el idSede enviado"
                })
            else
                disableService(Data, req, res);
        }
    })
    
}

const getAll = (req = request, res = response) => {
    getAllService(Data, req, res, { nombre: 1 });
}

module.exports = {
    create,
    getAll,
    update,
    disable
}