const { response, request } = require('express');

const Role = require("../models/role");

const createRole = async (req = request, res = response) => {
    const { rol } = req.body;

    const role = new Role({ rol });

    await role.save();

    res.json({
        success: true,
        message: "Role creado exitosamente",
        role
    })

}

module.exports = {
    createRole
}