const { response, request } = require('express');

const Data = require("../models/caracteristicaModel");
const Child = {
    "Asignatura": require("../models/joinCaracteristicas/caracteristicaAsignaturaModel"),
    "Curso": require("../models/joinCaracteristicas/caracteristicaCursoModel"),
    "Salon": require("../models/joinCaracteristicas/caracteristicaSalonModel"),
};

const { getAllService, disableService, updateService, createService } = require('../utils/transversalServices');

const create = (req = request, res = response) => {
      createService(Data, req, res);
}

const update = (req = request, res = response) => {
    updateService(Data, req, res);
}

const disable = (req = request, res = response) => {
    try{
        const { id, type } = req.body;
        
        Child[type].find({ $and: [ { estado: true }, { idCaracteristica: id }] }).exec((error, data)=>{
            if(error){
                return res.json({
                    codigo: 400,
                    success: false,
                    error
                })
            }else{
               if(data.length > 0)
                    return res.json({
                        codigo: 200,
                        success: false,
                        message: "Join creados con el idCaracteristica enviado"
                    })
                else
                    disableService(Data, req, res);
            }
        })
    }catch(error){
        return res.json({
            codigo: 400,
            success: false,
            error
        })
    }
    
}

const getAll = (req = request, res = response) => {
    getAllService(Data, req, res, { codigo: 1 });
}

module.exports = {
    create,
    getAll,
    update,
    disable
}