const { response } = require('express');
const bcryptjs = require('bcryptjs')

const Usuario = require('../models/usuario');

const { generarJWT } = require('../helpers/generar-jwt');


const login = async(req, res = response) => {

    const { correo, password } = req.body;

    try {
      
        // Verificar si el email existe
        const usuario = await Usuario.findOne({ correo });
        if ( !usuario ) {
            return res.status(400).json({
                success: false,
                msg: 'invalidEmail'
            });
        }

        // SI el usuario está activo
        if ( !usuario.estado ) {
            return res.status(400).json({
                success: false,
                msg: 'invalidEmail'
            });
        }

        // Verificar la contraseña
        const validPassword = bcryptjs.compareSync( password, usuario.password );
        if ( !validPassword ) {
            return res.status(400).json({
                success: false,
                msg: 'invalidPassword'
            });
        }

        // Generar el JWT
        const token = await generarJWT( usuario.id );

        res.json({
            success: true,
            usuario,
            token
        })

    } catch (error) {
        console.log(error)
        res.status(500).json({
            success: false,
            msg: 'Hable con el administrador'
        });
    }   

}



module.exports = {
    login
}
