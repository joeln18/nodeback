const { Schema, model } = require('mongoose');

const CursoSchema = Schema(
    {
        codigo: {
            type: String,
            required: [true, 'El código es obligatorio'],
            unique: true
        },
        idPrograma: {
            type: Schema.Types.ObjectId,
            ref: "Programa",
            required:  [true, 'El programa es obligatorio']
        },
        idAsignatura: {
            type: Schema.Types.ObjectId,
            ref: "Asignatura",
            required:  [true, 'La asignatura es obligatoria']
        },
        nombre: {
            type: String,
            required: [true, 'El nombre es obligatorio']
        },
        periodo: {
            type: Schema.Types.ObjectId,
            ref: "Periodo",
            required: [true, 'El nombre es obligatorio']
        },
        estado: {
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model( 'Curso', CursoSchema );