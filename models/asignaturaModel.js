const { Schema, model } = require('mongoose');

const AsignaturaSchema = Schema(
    {
        codigo: {
            type: String,
            required: [true, 'El código es obligatorio'],
            unique: true
        },
        idPrograma: {
            type: Schema.Types.ObjectId,
            ref: "Programa",
        },
        nombre: {
            type: String,
            required: [true, 'El nombre es obligatorio']
        },
        nivel: {
            enum: ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"],
            type: String,
            required: [true, 'El nivel es requrido']
        },
        horasSemanales: {
            type: Number,
            required: [true, 'Las horas semanales son obligatorias']
        },
        estado: {
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model( 'Asignatura', AsignaturaSchema );