const { Schema, model } = require('mongoose');

const SalonSchema = Schema(
    {
        codigo: {
            type: String,
            required: [true, 'El código es obligatorio'],
            unique: true
        },
        idSede:{
            type: Schema.Types.ObjectId,
            ref: "Sede",
        },
        idBloque: {
            type: Schema.Types.ObjectId,
            ref: "Bloque",
        },
        nombre: {
            type: String,
            required: [true, 'El nombre es obligatorio']
        },
        puestos: {
            type: Number,
            required: [true, 'La capacidad física es obligatoria']
        },
        puestosDisponibles: {
            type: Number,
            required: [true, 'La cantidad de puestos disponibles es obligatoria']
        },
        estado: {
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model( 'Salon', SalonSchema );