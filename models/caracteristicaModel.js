const { Schema, model } = require('mongoose');

const CaracteristicaSchema = Schema(
    {
        codigo: {
            type: String,
            required: [true, 'El código es obligatorio'],
            unique: true
        },
        nombre: {
            type: String,
            required: [true, 'La descripción es obligatoria']
        },
        type:{
            enum:["Asignatura", "Curso", "Salon"],
            type: String,
            required: true
        },
        estado:{
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model( 'Caracteristica', CaracteristicaSchema );