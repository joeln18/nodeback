const express = require('express');
const cors = require('cors');
const fileUpload = require('express-fileupload');

const { dbConnection } = require('../database/config');

class Server {

    constructor() {
        this.app  = express();
        this.port = process.env.PORT;

        this.arrayPaths = [
            {
                path: '/api/auth',
                route: require('../routes/auth')
            },
            {
                path: '/api/usuarios',
                route: require('../routes/usuarios')
            },
            {
                path: '/api/role',
                route: require('../routes/role')
            },
            {
                path: '/api/uploads',
                route: require('../routes/uploads')
            },
            {
                path: '/api/sedes',
                route: require('../routes/sedeRoutes')
            },
            {
                path: '/api/bloques',
                route: require('../routes/bloqueRoutes')
            },
            {
                path: '/api/salones',
                route: require('../routes/salonRoutes')
            },
            {
                path: '/api/caracteristicas',
                route: require('../routes/caracteristicaRoutes')
            },
            {
                path: '/api/programas',
                route: require('../routes/programaRoutes')   
            },
            {
                path: '/api/asignaturas',
                route: require('../routes/asignaturaRoutes')
            },
            {
                path: '/api/periodos',
                route: require('../routes/periodoRoutes')
            },
            {
                path: '/api/cursos',
                route: require('../routes/cursoRoutes')
            },
            {
                path: '/api/caracteristica/asignatura',
                route: require('../routes/joinCaracteristicas/caracteristicaAsignatura')
            },
            {
                path: '/api/caracteristica/curso',
                route: require('../routes/joinCaracteristicas/caracteristicaCurso')
            },
            {
                path: '/api/caracteristica/salon',
                route: require('../routes/joinCaracteristicas/caracteristicaSalon')
            }
        ]

        

        // Conectar a base de datos
        this.conectarDB();

        // Middlewares
        this.middlewares();

        // Rutas de mi aplicación
        this.routes();
    }

    async conectarDB() {
        await dbConnection();
    }


    middlewares() {

        // CORS
        this.app.use( cors() );

        // Lectura y parseo del body
        this.app.use( express.json() );

        // Directorio Público
        this.app.use( express.static('public') );

        //FileUploads
        this.app.use( fileUpload({
            useTempFiles : true,
            tempFileDir : '/tmp/',
            createParentPath: true
        }));

    }

    routes() {
        
        this.arrayPaths.forEach(item => {
            this.app.use(item.path, item.route)
        });
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log('Servidor corriendo en puerto', this.port );
        });
    }

}




module.exports = Server;
