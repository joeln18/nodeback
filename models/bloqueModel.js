const { Schema, model } = require('mongoose');

const BloqueSchema = Schema(
    {
        codigo: {
            type: String,
            required: [true, 'El código es obligatorio'],
            unique: true
        },
        idSede: {
            type: Schema.Types.ObjectId,
            ref: "Sede",
        },
        nombre: {
            type: String,
            required: [true, 'El nombre es obligatorio']
        },
        cantidadPisos: {
            type: String,
            required: [true, 'La cantidad de pisos es obligatoria']
        },
        cantidadSalones: {
            type: String,
            required: [true, 'La cantidad de salones es obligatoria'],
        },
        estado: {
            type: Boolean,
            required: false,
            default: true
        }
    },
    { 
        timestamps: true
    }
);

module.exports = model( 'Bloque', BloqueSchema );