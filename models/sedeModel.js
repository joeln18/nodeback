const { Schema, model } = require('mongoose');

const SedeSchema = Schema(
    {
        codigo: {
            type: String,
            required: [true, 'El código es obligatorio'],
            unique: true
        },
        nombre: {
            type: String,
            required: [true, 'El nombre es obligatorio']
        },
        ciudad: {
            type: String,
            required: [true, 'La ciudad es obligatoria']
        },
        ubicacion: {
            type: String,
            required: [true, 'La ubicación es obligatoria'],
        },
        estado: {
            type: Boolean,
            required: false,
            default: true
        }
    }, 
    { 
        timestamps: true 
    }
);

module.exports = model( 'Sede', SedeSchema );