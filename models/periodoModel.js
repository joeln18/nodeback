const { Schema, model } = require('mongoose');

const PeriodoSchema = Schema(
    {
        codigo: {
            type: String,
            required: [true, 'El código es obligatorio'],
            unique: true
        },
        nombre: {
            type: String,
            required: [true, 'La descripción es obligatoria']
        },
        estado:{
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model( 'Periodo', PeriodoSchema );