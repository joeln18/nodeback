const { Schema, model } = require("mongoose");


const CaracteristicaSalon = Schema(
    {
        idCaracteristica: {
            type: Schema.Types.ObjectId,
            ref: "Caracteristica"
        },
        idSalon:{
            type: Schema.Types.ObjectId,
            ref: "Salon"
        },
        estado:{
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model("CaracteristicaSalon", CaracteristicaSalon);