const { Schema, model } = require("mongoose");


const CaracteristicaAsignatura = Schema(
    {
        idCaracteristica: {
            type: Schema.Types.ObjectId,
            ref: "Caracteristica"
        },
        idAsignatura:{
            type: Schema.Types.ObjectId,
            ref: "Asignatura"
        },
        estado:{
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model("CaracteristicaAsignatura", CaracteristicaAsignatura);
