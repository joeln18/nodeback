const { Schema, model } = require("mongoose");


const CaracteristicaCurso = Schema(
    {
        idCaracteristica: {
            type: Schema.Types.ObjectId,
            ref: "Caracteristica"
        },
        idCurso:{
            type: Schema.Types.ObjectId,
            ref: "Curso"
        },
        estado:{
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model("CaracteristicaCurso", CaracteristicaCurso);