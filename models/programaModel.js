const { Schema, model } = require('mongoose');

const ProgramaSchema = Schema(
    {
        codigo: {
            type: String,
            required: [true, 'El código es obligatorio'],
            unique: true
        },
        nombre: {
            type: String,
            required: [true, 'El nombre es obligatorio']
        },
        nivelProfesional: {
            type: String,
            required: [true, 'El nível profesional es obligatorio'],
            enum: ['PROFESIONAL', 'TECNOLOGO', 'TECNICO']
        },
        estado: {
            type: Boolean,
            required: false,
            default: true
        }
    },
    {
        timestamps: true
    }
);

module.exports = model( 'Programa', ProgramaSchema );